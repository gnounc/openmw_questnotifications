local ui = require('openmw.ui')
local self = require('openmw.self')
local core = require('openmw.core')
local types = require('openmw.types')
local async = require('openmw.async')


--done
--add new town discovered
--add sound effect
--add quest started
--add quest finished

--todo
--add ui popup
--add check for icon
--add default icon
--add default questname for empties

--joined faction

local doOnce = false

local time = 0
local lastUpdate = 0
local prevQuestLen = 0
local oldQuests = nil

local dbg = true


local function d_message(msg)
	if not dbg then return end

	ui.showMessage(msg)
end

local function d_print(fname, msg)
	if not dbg then return end

	if fname == nil then
		fname = "\x1b[35mnil"
	end

	if msg == nil then
		msg = "\x1b[35mnil"
	end

	print("\n\t\x1b[33;3m" .. fname .. "\n\t\t\x1b[33;3m" .. msg .. "\n\x1b[39m")
end

function firstToUpper(str)
    return (str:gsub("^%l", string.upper))
end

local function getQuestName(str)

	local qname = ""
	for token in string.gmatch(str, "([^_]+)") do
		if not string.match(token, "[0-9]") then
			if #token > 2 and not string.match(token, "town") then
				qname = qname .. firstToUpper(token) .. " "
			end
		end
	end

	return qname
end

local notificationType = {
	skill = "{s} leveled up", 
	location = "{s} discovered", 
	levelup = "Leveled up {s}", 
	started = "Quest Started {s}", 
	finished = "Quest Completed {s}" 
}

local function notify(quest, notifyType)
	local notificationSound = "sound/fx/inter/levelup.wav"
	local message = notifyType:gsub("{s}", quest)

	d_print("notify", message)

	async:newUnsavableSimulationTimer(0.2, function()  
		ui.showMessage(message)
		core.sound.playSoundFile3d(notificationSound, self)	
	end)
end

local function onQuestUpdate(questId, stage)

	local quest = types.Player.quests(self)[questId]

	if quest.started and not quest.finished then
		if string.match(quest.id, "town") then
			notify(getQuestName(quest.id), notificationType["location"])
		else
			notify(getQuestName(quest.id), notificationType["started"])
		end
	elseif quest.finished then
		notify(getQuestName(quest.id), notificationType["finished"])
	end
end


return {engineHandlers = { onUpdate = onUpdate, onQuestUpdate = onQuestUpdate } }

