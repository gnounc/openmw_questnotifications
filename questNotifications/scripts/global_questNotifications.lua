local core = require('openmw.core')
local types = require("openmw.types")
local world = require("openmw.world")

local function payCharge(data)
	local currentCharge = types.Item.getEnchantmentCharge(data.item)
	types.Item.setEnchantmentCharge(data.item, currentCharge - data.charge)
end

local function getPlayerRace(player)
	print("getrace event recieved in global.lua")
	player:sendEvent("event_getRace", types.NPC.record(player).race)
end


return { eventHandlers = { getPlayerRace = getPlayerRace } }
